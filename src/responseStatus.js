module.exports = {
    ok: 200,
    notFound: 404,
    forbidden: 403,
    badRequest: 400,
    internalServerError: 500
};
